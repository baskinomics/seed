"""The long awaited implementation...

Use a init script to download and install julia.
Mount a Julia container.

- Fonts
    - Hasklig NF
    - Source Sans 3
    - Source Serif Pro
- Software
    - vim
    - bat
    - lsd
    - rg
    - tldeer
    - fd
    - Starship.rs
    - Visual Studio Code
- Programming Languages
    - Java
    - Python
    - Julia
    - Rust
- Configuration
    - SSH
    - Git
    - zsh
    - VSCode
- Administration
    - Package manager update and upgrade
"""
module Seed

end